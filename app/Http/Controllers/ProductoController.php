<?php

namespace App\Http\Controllers;

use App\Producto;
use Illuminate\Http\Request;
use DataTables;
use Validator;
use Auth;
use File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use URL;

class ProductoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Columnas Datatable JSON - AJAX
        $data['columnas_datatable_json']=array(['data'=>'id','name'=> 'producto.id'],['data'=>'nombre','name'=>'producto.nombre'],['data'=>'descripcion','name'=>'producto.descripcion'],['data'=>'foto','name'=>'foto'],['data'=>'precio','name'=>'precio'],['data'=>'acciones','name'=>'acciones','orderable'=>false,'searchable'=>false]);

        $data['url_datatable_ajax']=url('producto/listing-datatable');

        return view('producto.index')->With($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['tipo_view']='create';

        return view('producto.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
        ]);            

        if($validation->fails()) { return back()->withErrors($validation)->withInput();}

        $id=Auth::user()->id;

        $producto=new Producto();
        $producto->nombre=$request->nombre;
        $producto->descripcion=$request->descripcion;
        $producto->precio=$request->precio;
        $producto->created_by=$id;
        $producto->estado=1;
        $producto->save();

        $url_foto='';
        if($request->file('foto')){        
            $nombre_archivo = $id.'_'.Carbon::now()->format('Y_m_d_h_i_s').'.'.$request->file('foto')->getClientOriginalExtension();
            $url_foto='producto/'.$id.'/'.$nombre_archivo;
            Storage::disk('public')->put('producto/'.$id.'/'.$nombre_archivo, File::get($request->file('foto')), 'public');
        }

        $producto->foto=$url_foto;
        $producto->update();


        $response = [
            'icono' => 'success',
            'titulo' => 'Producto creado con éxito.',
            'mensaje' => 'El registro se guardo satisfactoriamente',
        ];


        return redirect(url('producto'))->with('response', $response);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['producto']=Producto::find($id);
        return view('producto.show')->with($data);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['tipo_view']='edit';
        $data['producto']=Producto::find($id);

        return view('producto.create')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => 'required',
            'precio' => 'required',
        ]);            

        if($validation->fails()) { return back()->withErrors($validation)->withInput();}

        $producto=Producto::find($id);
        $producto->nombre=$request->nombre;
        $producto->descripcion=$request->descripcion;
        $producto->precio=$request->precio;
        $producto->updated_by=Auth::user()->id;
        $producto->estado=1;

        $url_foto='';
        if($request->file('foto')){        
            $nombre_archivo = $id.'_'.Carbon::now()->format('Y_m_d_h_i_s').'.'.$request->file('foto')->getClientOriginalExtension();
            $url_foto='producto/'.$id.'/'.$nombre_archivo;
            Storage::disk('public')->put('producto/'.$id.'/'.$nombre_archivo, File::get($request->file('foto')), 'public');
        }
        $producto->foto=$url_foto;
        $producto->update();

        $response = [
            'icono' => 'success',
            'titulo' => 'Producto actualizado con éxito.',
            'mensaje' => 'El registro se actualizo satisfactoriamente',
        ];


        return redirect('producto')->with('response', $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto=Producto::find($id);
        $producto->estado=0;
        $producto->update();

        return response()->json(['eliminado'=>true]);
    }
    public function datatable(Request $request){

        $productos=Producto::select(['id','nombre','descripcion','foto','precio'])->where('estado',1);

        return Datatables::of($productos)
        ->editColumn('id', function($producto) {
            $html='<a href="'.url('producto/'.$producto->id.'').'">'.$producto->id.'</a>'; 
          return $html;
        })
        ->editColumn('nombre', function($producto) {
            $html='<a href="'.url('producto/'.$producto->id.'').'">'.$producto->nombre.'</a>'; 
          return $html;
        })
        ->editColumn('acciones', function($producto) {

            $html='<a class="btn btn-success " href="'.url('producto/'.$producto->id.'').'"><i class="fa fa-eye"></i></a>'.
                      '<a class="btn btn-danger"  onclick="eliminar('.$producto->id.')"><i class="fa fa-trash"></i></a>'.
                      '<a class="btn btn-primary" href="'.url('producto/'.$producto->id.'/edit').'"><i class="fa fa-edit"></i></a>';
          return $html;
        })
        ->editColumn('foto', function($producto) {
           $html=""; 
           if($producto->foto)
           {
                 $html.='<img src="'.asset('storage/'.$producto->foto).'" width="100px" height="100px"></img>';
           }
           else
           {
                $html='<span class="badge badge-danger">No Registra</span>';
           }
           return $html."";
        })
        ->rawColumns(['id','acciones','nombre','foto'])  //Sirve para imprimir Columnas Crudas (Columna que contienen HTML)
        ->make(true);
    
    }
}
