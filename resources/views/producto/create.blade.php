@extends('layouts.app')

@section('css_level')
		<!-- FileInput -->
	<link href="{{mix('/css/fileinput.css')}}" rel="stylesheet" />
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center">


		<div class="col-md-12 bg-light rounded-lg" >
				@if($tipo_view=='edit')
					<form action="{{url('producto/'.$producto->id)}}" id="form-producto" method="POST" id="form-global" files="true" enctype="multipart/form-data" >
	        		<input type="hidden" name="_method" value="PUT" id="_method"> 
				@else
					<form action="{{route('producto.store')}}" id="form-producto" method="POST" id="form-global" files="true" enctype="multipart/form-data" >
				@endif
	        	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token"> 
				<hr>
				<div class="row">	

			 		<div class="form-group col-md-6">
						<label for="nombre">Nombre <span style="color:red">(*)</span></label>
						<input type="text" class="form-control" value='{{(isset($producto)?$producto->nombre:"")}}' id="nombre" name="nombre" required>
					</div>
			 		<div class="form-group col-md-6">
						<label for="precio">Precio <span style="color:red">(*)</span></label>
						<input type="text" class="form-control" value='{{(isset($producto)?$producto->precio:"")}}' id="precio" name="precio" required>
					</div>
					<div class="form-group col-md-12">
						<label for="descripcion">Descripción <span style="color:red">(*)</span></label>
						<textarea class="form-control" value='{{(isset($producto)?$producto->descripcion:"")}}' id="descripcion" name="descripcion" required rows="3">{{(isset($producto)?$producto->descripcion:"")}}</textarea>
					</div>
					<div class="form-group col-md-6">
						@if(isset($producto))
							@if($producto->foto)
								<img src="{{ asset('storage/'.$producto->foto)}}" width="150px" height="150px"><br>
							@endif
						@endif
						<label for="foto">Foto </label>
						<input type="file" accept=".png, .jpeg, .jpg, .gif" class="form-control input_adjundtar_archivo"  id="foto" name="foto" ></input>
					</div>

	 			</div>
				<div class="form-group">
					<a class="btn btn-warning" href="{{ url()->previous() }}" ><i class="fa fa-reply-all"></i> Regresar</a>	
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
					<button type="reset" class="btn btn-danger"><i class="fa fa-times-circle"></i> Cancelar</button>
				</div>
				<br>
			</form>
		</div>


    </div>
</div>
@endsection


<!-- Css -->
@section('js_level')
	<!-- Plugin Fileinput -->
	<script src="{{mix('/js/fileinput.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/additional-methods.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/localization/messages_es.min.js"></script>
	<!-- FileInput -->
	<link href="{{mix('/css/fileinput.css')}}" rel="stylesheet" />
	<script type="text/javascript">

		//Validation Jquery
	    $('#form-producto').validate();


	    // Plugin para Input's archivos
	    $('.input_adjundtar_archivo').fileinput({
	        theme: 'fa',
	        language: 'es',
	        allowedFileExtensions: ['jpg', 'png', 'gif'],
	        showUpload: false,
	        showCaption: false,  
	        showUploadedThumbs: false, 
	        showRemove: true,
	        dropZoneEnabled: false,
	        maxFileCount: 10,
	        mainClass: "input-group-lg"
	    });

		@if(session('response'))
			 Swal.fire(
				  '{{ session("response")["titulo"] }}',
				  '{{ session("response")["mensaje"]}}',
				  '{{ session("response")["icono"]}}'
				);
		  {{session()->forget('response')}}
		@endif
	</script>

@endsection