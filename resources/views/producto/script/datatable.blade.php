<script type="text/javascript">
 
  let language =
  {
    'sProcessing': 'Procesando...',
    'sLengthMenu': 'Mostrar _MENU_ registros',
    'sZeroRecords': 'No se encontraron resultados',
    'sEmptyTable': 'Ningún dato disponible en esta tabla',
    'sInfo': 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
    'sInfoEmpty': 'Mostrando registros del 0 al 0 de un total de 0 registros',
    'sInfoFiltered': '(filtrado de un total de _MAX_ registros)',
    'sInfoPostFix': '',
    'sSearch': 'Buscar:',
    'sUrl': '',
    'sInfoThousands': ',',
    'sLoadingRecords': 'Cargando...',
    'oPaginate': {
        'sFirst': 'Primero',
        'sLast': 'Último',
        'sNext': 'Siguiente',
        'sPrevious': 'Anterior'
    },
    'oAria': {
        'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
        'sSortDescending': ': Activar para ordenar la columna de manera descendente'
    }
  };

  function renderizar_datatable(id_datatable,columns,url_ajax)
  {

      var token = '{{ csrf_token() }}';
      var datatable = $(id_datatable).DataTable({
        processing: true,
        serverSide: true,
        search: true,
        ordering: true,
        scrollX:true,
        searchable:true,
        searching: true,
        language: language,
        ajax: {
          url: url_ajax,
          headers: {'X-CSRF-TOKEN': token},
          method: 'POST',
          data: (d) => {
            if(typeof dataFilterDatatable === 'function') {
              let data_filter = dataFilterDatatable();
              for (var item in data_filter) {
                d[item] = data_filter[item]
              }
            }
          }
        },
        columns: columns,
        autoWidth: true,
      });

      return datatable;
  }
</script>