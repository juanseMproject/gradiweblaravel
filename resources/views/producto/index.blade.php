@extends('layouts.app')
<!-- Css -->
@section('css_level')
	<!-- Datatable -->
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet"></link>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">

		<div class="col-md-12 bg-light rounded-lg">
      		<br>
			<h2 style="font-weight: bold;" class="text-info"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Listado de Productos</h2>
			<hr>
	      	<div class="text-right">
				<a class="btn btn-primary" href="{{route('producto.create')}}"  style="font-weight: bold;">Crear Producto</a>
			</div>
          	<br>
	        <div class="card border-success mb-3">
	              <div class="card-header text-white bg-success"><h4 style="font-weight: bold;"><i class="fa fa-bars" aria-hidden="true"></i> Listado de Productos<h4></div>
	              <div class="card-body">
	                	<div class="col-md-12">                    		
    						<table class="table table-striped no-footer"  style="width:100%" id="datatable">
    							<thead>
    								<tr>
    									<th>Id</th>
    									<th>Nombre</th>
    									<th>Descripción</th>
    									<th>Foto</th>
    									<th>Precio</th>
    									<th>Acciones</th>
    								</tr>					
    							</thead>
    							<tbody>
    							</tbody>
    						</table>
	                	</div>
	              </div>
	          </div>
	      	<br>
		</div>


    </div>
</div>
@endsection


<!-- Css -->
@section('js_level')

	<!-- Datatable Script's-->
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<!-- Include donde se encuentra la inicializacion del datatable -->

	<script type="text/javascript">

	 let language =
	  {
	    'sProcessing': 'Procesando...',
	    'sLengthMenu': 'Mostrar _MENU_ registros',
	    'sZeroRecords': 'No se encontraron resultados',
	    'sEmptyTable': 'Ningún dato disponible en esta tabla',
	    'sInfo': 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
	    'sInfoEmpty': 'Mostrando registros del 0 al 0 de un total de 0 registros',
	    'sInfoFiltered': '(filtrado de un total de _MAX_ registros)',
	    'sInfoPostFix': '',
	    'sSearch': 'Buscar:',
	    'sUrl': '',
	    'sInfoThousands': ',',
	    'sLoadingRecords': 'Cargando...',
	    'oPaginate': {
	        'sFirst': 'Primero',
	        'sLast': 'Último',
	        'sNext': 'Siguiente',
	        'sPrevious': 'Anterior'
	    },
	    'oAria': {
	        'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
	        'sSortDescending': ': Activar para ordenar la columna de manera descendente'
	    }
	  };

	  function renderizar_datatable(id_datatable,columns,url_ajax)
	  {

	      var token = '{{ csrf_token() }}';
	      var datatable = $(id_datatable).DataTable({
	        processing: true,
	        serverSide: true,
	        search: true,
	        ordering: true,
	        scrollX:true,
	        searchable:true,
	        searching: true,
	        language: language,
	        ajax: {
	          url: url_ajax,
	          headers: {'X-CSRF-TOKEN': token},
	          method: 'POST',
	          data: (d) => {
	            if(typeof dataFilterDatatable === 'function') {
	              let data_filter = dataFilterDatatable();
	              for (var item in data_filter) {
	                d[item] = data_filter[item]
	              }
	            }
	          }
	        },
	        columns: columns,
	        autoWidth: true,
	      });

	      return datatable;
	  }


	  function eliminar(id)
	  {   

  		Swal.fire({
	        title: 'Estas seguro de eliminar este Producto?',
	        text: "La información de este Producto se borrara",
	        icon: 'warning',
	        showCancelButton: true,
	        confirmButtonText: 'Si',
	        cancelButtonText: 'No',
	        reverseButtons: true
	      }).then((result) => {
	        if(result) 
	        { 
	           $.ajax({
	              url: "{{ url('producto') }}/" + id,
	              method: 'DELETE',
	              headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
	              success: function(result){  
	                  Swal.fire('Producto Eliminado','Se elimino la información de este Producto ','success');
	                  window.location.href='{{url("producto")}}';
	              },
	              error: function (data) {
	                  Swal.fire('Producto No Eliminado','El propietario no pudo ser Eliminado','error');
	              }
	          });

	        } 
	      });

	  }

	$(document).ready(function(){
		var columns=JSON.parse('{!! json_encode($columnas_datatable_json) !!}');
		var url_datatable_ajax='{{$url_datatable_ajax}}';
		var datatable=renderizar_datatable('#datatable',columns,url_datatable_ajax);
	});

	@if(session('response'))
		 Swal.fire(
			  '{{ session("response")["titulo"] }}',
			  '{{ session("response")["mensaje"]}}',
			  '{{ session("response")["icono"]}}'
			);
	  {{session()->forget('response')}}
	@endif
	</script>

@endsection