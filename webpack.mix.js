const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');


//Fileinput
mix.styles(['public/assets/plugin-subir-archivos/js/fileinput.js',
			'public/assets/plugin-subir-archivos/themes/fa/theme.js',
			'public/assets/plugin-subir-archivos/js/locales/es.js'], 'public/js/fileinput.js');
mix.styles(['public/assets/plugin-subir-archivos/css/fileinput.css'],'public/css/fileinput.css');
