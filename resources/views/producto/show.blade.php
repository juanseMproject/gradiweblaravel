@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">

		<div class="col-md-12 bg-light rounded-lg" >
				<hr>
				<div class="row">		

			 		<div class="form-group col-md-6">
						<h4>Nombre:</h4>
						<span>{{(isset($producto)?$producto->nombre:"No Registra")}}</span>
					</div>
			 		<div class="form-group col-md-6">
						<h4>Precio:</h4>
						<span>{{(isset($producto)?$producto->precio:"No Registra")}}</span>
					</div>
					<div class="form-group col-md-6">
						<h4>Descripción:</h4>
						<span>{{(isset($producto)?$producto->precio:"No Registra")}}</span>
					</div>
					<div class="form-group col-md-6">
						<h4>Foto:</h4>
						@if(isset($producto))
							@if($producto->foto)
								<img src="{{ asset('storage/'.$producto->foto)}}" width="150px" height="150px"><br>
							@else
								<span>{{(isset($producto)?$producto->precio:"No Registra")}}</span>
							@endif
						@endif
					</div>

	 			</div>
				<div class="form-group">
					<a class="btn btn-warning" href="{{ url()->previous() }}" ><i class="fa fa-reply-all"></i> Regresar</a>	
                    <a class="btn btn-danger"  onclick="eliminar({{$producto->id}})"><i class="fa fa-trash"></i> Eliminar</a>
                    <a class="btn btn-primary" href="{{url('producto/'.$producto->id.'/edit')}}"><i class="fa fa-edit"></i> Editar</a>
				</div>
				<br>
			</form>
		</div>
    </div>
</div>
@endsection


<!-- Css -->
@section('js_level')
	<script type="text/javascript">
		@if(session('response'))
			 Swal.fire(
				  '{{ session("response")["titulo"] }}',
				  '{{ session("response")["mensaje"]}}',
				  '{{ session("response")["icono"]}}'
				);
		  {{session()->forget('response')}}
		@endif
	</script>

@endsection